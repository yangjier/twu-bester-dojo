package bester;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BestableTest {

    @Test
    public void rectangleAShouldBetterThanRectangleB() {
        Rectangle rectangleA = new Rectangle(4,4);
        Rectangle rectangleB = new Rectangle(2,2);
        assertEquals(true, rectangleA.isBetterThan(rectangleB));

    }
    @Test
    public void rectangleBShouldBetterThanRectangleA() {
        Rectangle rectangleA = new Rectangle(4,4);
        Rectangle rectangleB = new Rectangle(2,2);
        assertEquals(false, rectangleB.isBetterThan(rectangleA));

    }
}
