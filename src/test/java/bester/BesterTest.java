package bester;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BesterTest {

    @Test
    public void theBesterMustBeItself() {
        Bester bester = new Bester();
        Rectangle rectangle = new Rectangle(4, 1);
        ArrayList<Bestable> rectangleList = new ArrayList<>();

        rectangleList.add(rectangle);
        assertEquals(rectangle, bester.best(rectangleList));
    }

    @Test
    public void shouldReturnNullWhenThereIsEmpty() {
        Bester bester = new Bester();
        ArrayList<Bestable> rectangleList = new ArrayList<>();

        assertEquals(null, bester.best(rectangleList));
    }

    @Test
    public void shouldReturnTheFirstOne() {
        Bester bester = new Bester();
        Rectangle rectangleA = new Rectangle(4, 1);
        Rectangle rectangleB = new Rectangle(2, 2);
        ArrayList<Bestable> rectangleList = new ArrayList<>();

        rectangleList.add(rectangleA);
        rectangleList.add(rectangleB);
        assertEquals(rectangleA, bester.best(rectangleList));
    }

    @Test
    public void shouldReturnMaxOne() {
        Bester bester = new Bester();
        Rectangle rectangleA = new Rectangle(4, 1);
        Rectangle rectangleB = new Rectangle(2, 2);
        Rectangle rectangleC = new Rectangle(10, 10);
        ArrayList<Bestable> rectangleList = new ArrayList<>();

        rectangleList.add(rectangleA);
        rectangleList.add(rectangleB);
        rectangleList.add(rectangleC);
        assertEquals(rectangleC, bester.best(rectangleList));
    }

    @Test
    public void shouldReturnMaxOneForCookie() {
        Bester bester = new Bester();
        Cookie cookieA = new Cookie(10);
        Cookie cookieB = new Cookie(100);
        Cookie cookieC = new Cookie(0);
        ArrayList<Bestable> cookieList = new ArrayList<>();

        cookieList.add(cookieA);
        cookieList.add(cookieB);
        cookieList.add(cookieC);
        assertEquals(cookieB, bester.best(cookieList));
    }
}
