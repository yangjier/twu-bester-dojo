package bester;

import java.util.ArrayList;

public class Bester {

    public Bestable best(ArrayList<Bestable> bestables) {
        if(bestables.isEmpty()){
            return null;
        }

        int maxIndex = 0;

        for(Bestable bestable:bestables){
            if(bestable.isBetterThan(bestables.get(maxIndex))){
                maxIndex = bestables.indexOf(bestable);
            }
        }

        return bestables.get(maxIndex);
    }
}
